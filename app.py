import os

import openai
from flask import Flask, redirect, render_template, request, url_for
from speech_recognition import *

history = [""]

app = Flask(__name__)
openai.api_key = os.getenv("OPENAI_API_KEY")


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        prompt = recognize_from_microphone()
        response = openai.Completion.create(
            model="text-davinci-003",
            prompt=generate_prompt(prompt),
            temperature=0.6,
            max_tokens=700,
        )
        return redirect(url_for("index", result=response.choices[0].text, prompt=prompt))

    print(history)
    result = request.args.get("result")
    prompt = request.args.get("prompt")
    if prompt:
        history.append("A: " + prompt)
    if result:
        history.append("B: " + result)
    limit_history()
    return render_template("index.html", result=result, prompt=prompt)

@app.route("/cleancache/", methods=["POST"])
def clean_cache():
    history = [""]
    return redirect(url_for("index"))

def generate_prompt(prompt):
    past = generate_past()
    if not prompt:
         prompt = ""
    now = "A: " + prompt + "\n"
    future = "B: "
    return (past + now + future)

def generate_past():
    past = ""
    for i in range (0, len(history)):
            past = past + history[i] + "\n"
    return (past)

def limit_history():
    while len(history) > 40:
        history.pop(0)
